import com.abhi.CustomHashMap;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

public class TestHashMap {
    CustomHashMap<Integer, String > hmp;
    CustomHashMap<Integer, String> newhmp;

    @Before
    public void setup(){
        hmp = new CustomHashMap<>();
        newhmp = new CustomHashMap<>();
    }

    @Test
    public void putTest(){
        assertEquals(hmp.put(1,"test1"), "test1");
    }

    @Test
    public void getTest(){
        hmp.put(1,"test1");
        assertEquals(hmp.get(1), "test1");
    }


    @Test
    public void putDuplicateKeyTest(){
        hmp.put(1,"test1");
        hmp.put(1,"test2");
        assertEquals(hmp.get(1), "test2");
    }

    @Test
    public void putDuplicateValueTest(){
        hmp.put(1,null);
        assertEquals(hmp.get(1), null);
    }

    @Test
    public void putNullKeyTest(){
        hmp.put(null, "test1");
        assertEquals(hmp.get(null), "test1");
    }

    @Test
    public void sizeTest(){
        hmp.put(1,"test1");
        int size = hmp.size();
        assertEquals(size,1);
    }

    @Test
    public void isEmptyTest(){
        assertTrue(hmp.isEmpty());
    }

    @Test
    public void containsKeyTest(){
        hmp.put(1,"test1");
        assertTrue(hmp.containsKey(1));
        assertFalse(hmp.containsKey(5));
    }

    @Test
    public void containsValueTest(){
        hmp.put(1,"test1");
        assertTrue(hmp.containsValue("test1"));
        assertFalse(hmp.containsValue("test5"));
    }

    @Test
    public void removeTest(){
        hmp.put(1,"test1");
        hmp.remove(1);
        assertFalse(hmp.containsKey(1));
    }

    @Test
    public void putAllTest(){
        hmp.put(1,"test1");
        hmp.put(2,"test2");
        newhmp.putAll(hmp);
        assertEquals(newhmp.size(), 2);
        assertEquals(hmp.get(1), newhmp.get(1));
        assertEquals(hmp.get(2), newhmp.get(2));
    }

    @Test
    public void clearTest(){
        hmp.put(1,"test1");
        assertFalse(hmp.isEmpty());
        hmp.clear();
        assertTrue(hmp.isEmpty());
    }

    @Test
    public void keySetTest(){
        hmp.put(1,"test1");
        hmp.put(2,"test2");
        hmp.put(3,"test3");
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        assertEquals(set, hmp.keySet());
    }

    @Test
    public void valuesTest(){
        hmp.put(1,"test1");
        hmp.put(2,"test2");
        hmp.put(3,"test3");
        Set<String > set = new HashSet<>();
        set.add("test1");
        set.add("test2");
        set.add("test3");
        assertEquals(set, hmp.values());
    }
}
