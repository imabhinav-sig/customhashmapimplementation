package com.abhi;

import com.abhi.CustomHashMap;

public class Main {
    public static void main(String[] args) {
        CustomHashMap<Integer, String> hm = new CustomHashMap<Integer, String>();
        hm.put(1, "a");
        hm.put(2, "b");
        hm.put(3, "c");
        hm.put(4, "d");
        hm.put(5, "e");
        hm.put(6, "f");
//        for(Map.Entry<Integer,String> e : hm.entrySet()){
//            System.out.println("Key : " + e.getKey() + " Value : " + e.getValue());
//        }
        System.out.println(hm);
        System.out.println(hm.get(2));
        System.out.println(hm.remove(1));
        hm.resize(32);
        System.out.println(hm.get(3));
    }
}
