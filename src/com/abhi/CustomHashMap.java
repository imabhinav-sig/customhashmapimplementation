package com.abhi;

import java.util.*;
import java.util.stream.Collectors;

public class CustomHashMap<K,V> implements Map<K,V> {

    /**
     * The default initial capacity - MUST be a power of two.
     */
    static final int DEFAULT_INITIAL_CAPACITY = 16;

    /**
     * The maximum capacity,
     * MUST be a power of two <= 1<<30.
     */
    static final int MAXIMUM_CAPACITY = 1 << 30;

    /**
     * The load factor used when none specified in constructor.
     */
    static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * The table, resized as necessary. Length MUST Always be a power of two.
     */
    ArrayList<Entry<K,V>> entryTable;

    /**
     * The number of key-value mappings contained in this map.
     */
    transient int size;

    /**
     * The next size value at which to resize (capacity * load factor).
     */
    int threshold;

    /**
     * The load factor for the hash table.
     */
    final float loadFactor;

    //======================= Constructors =========================
    /**
     * Constructs an empty HashMap with the default initial capacity
     * (16) and the default load factor (0.75).
     */
    public CustomHashMap() {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        threshold = (int)(DEFAULT_INITIAL_CAPACITY * DEFAULT_LOAD_FACTOR);
        entryTable = new ArrayList<>(Collections.nCopies(DEFAULT_INITIAL_CAPACITY, null));
    }

    /**
     * Constructs an empty HashMap with the specified initial
     * capacity and load factor.
     *
     * @param  initialCapacity the initial capacity
     * @param  loadFactor      the load factor
     * @throws IllegalArgumentException if the initial capacity is negative
     *         or the load factor is non-positive
     */
    public CustomHashMap(int initialCapacity, float loadFactor) {
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal initial capacity: "
                    + initialCapacity + ", initial capacity cannot be be negative");

        if (initialCapacity > MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;

        if (loadFactor <= 0 || Float.isNaN(loadFactor))
            throw new IllegalArgumentException("Illegal load factor: " + loadFactor);

        // Find a power of 2 greater than initial capacity
        int capacity = 1;
        while (capacity < initialCapacity)
            capacity <<= 1;

        this.loadFactor = loadFactor;
        threshold = (int)(capacity * loadFactor);
        entryTable = new ArrayList<>(capacity);
    }

    /**
     * Constructs an empty HashMap with the specified initial
     * capacity and the default load factor (0.75).
     *
     * @param  initialCapacity the initial capacity.
     * @throws IllegalArgumentException if the initial capacity is negative.
     */

    public CustomHashMap(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    /**
     * Constructs a new HashMap with the same mappings as the
     * specified Map. The HashMap is created with
     * default load factor (0.75) and an initial capacity sufficient to
     * hold the mappings in the specified Map.
     *
     * @param   m the map whose mappings are to be placed in this map
     * @throws  NullPointerException if the specified map is null
     */
    public CustomHashMap(Map<? extends K, ? extends V> m) {
        this(Math.max((int)(m.size() / DEFAULT_LOAD_FACTOR) + 1, DEFAULT_INITIAL_CAPACITY), DEFAULT_LOAD_FACTOR);
        for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
            put(e.getKey(), e.getValue());
    }

    //=======================Internal Utilities======================

    /**
     * @param h hash code for key
     * @param length length of table
     *
     * @return the index for hash code h.
     */
    static int indexFor(int h, int length) {
        return h & (length-1);
    }

    /**
     * Returns the number of key-value mappings in this map.
     *
     * @return the number of key-value mappings in this map
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Returns if this map contains no key-value mappings
     *
     * @return the number of key-value mappings
     */
    @Override
    public boolean isEmpty() {
        return size==0;
    }

    /**
     * @param   key   The key whose presence in this map is to be tested
     * @return true if this map contains a mapping for the specified key.
     */
    @Override
    public boolean containsKey(Object key) {
        return get(key) != null;
    }

    /**
     * Returns true if specified value is present in the map
     *
     * @param   value   The value whose presence in this map is to be tested
     * @return true if this map contains a mapping for the specified value.
     */
    @Override
    public boolean containsValue(Object value) {
        for (Entry<K, V> kvEntry : entryTable)
            for (Entry<K, V> e = kvEntry; e != null; e = e.next)
                if (value.equals(e.value))
                    return true;
        return false;
    }

    /**
     * Returns the value for the specified key in the map
     * or null if there is no such key
     *
     * @param key The key whose value in this map is to be fetched
     * @return the value for the specified key in the map
     */
    @Override
    public V get(Object key) {
        int hash = (key==null)?0:key.hashCode();
        int index = indexFor(hash, entryTable.size());

        for (Entry<K,V> e = entryTable.get(index); e!=null; e=e.next) {
            if (e.hash == hash && ((key!=null && key.equals(e.key)) || key==e.key))
                return e.value;
        }

        return null;
    }

    /**
     * Associated the specified value with the specified key in this map
     * @param key key with which value is to be associated
     * @param value value to be associated
     * @return the previous value associated or null if no previous mapping
     */
    @Override
    public V put(K key, V value) {
        int hash = (key==null)?0:key.hashCode();
        int i = indexFor(hash, entryTable.size());
        for (Entry<K,V> e = entryTable.get(i); e != null; e = e.next) {
            if (e.hash == hash && key.equals(e.key)) {
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
        }

        Entry<K,V> e = entryTable.get(i);
        entryTable.set(i,new Entry(hash, key, value, e));
        if (size++ >= threshold)
            resize(2 * entryTable.size());
        return entryTable.get(i).value;
    }

    /**
     * Resize the map to hold mappings
     *
     * @param newCapacity the new capacity, Must be a power of 2
     */
    void resize(int newCapacity) {
        if (entryTable.size() == MAXIMUM_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return;
        }

        ArrayList<Entry<K,V>> resizedTable = new ArrayList<>(Collections.nCopies(newCapacity, null));
        for (int j = 0; j < entryTable.size(); j++) {
            Entry<K,V> e = entryTable.get(j);
            if (e != null) {
                entryTable.set(j, null);
                do {
                    Entry<K,V> next = e.next;
                    int i = indexFor(e.hash, newCapacity);
                    e.next = resizedTable.get(i);
                    resizedTable.set(i,e);
                    e = next;
                } while (e != null);
            }
        }

        entryTable = resizedTable;
        threshold = (int)(newCapacity * loadFactor);
    }

    /**
     * Removes mapping for the specified key from the map
     *
     * @param key key whose mapping is to be removed from the map
     * @return the previous value associated or null if there was no mapping
     */
    @Override
    public V remove(Object key) {
        int hash = (key == null) ? 0 : key.hashCode();
        int i = indexFor(hash, entryTable.size());
        Entry<K,V> prev = entryTable.get(i);
        Entry<K,V> e = prev;

        while (e != null) {
            Entry<K,V> next = e.next;
            assert key != null;
            if (e.hash == hash && key.equals(e.key)) {
                size--;
                if (prev == e)
                    entryTable.set(i,next);
                else
                    prev.next = next;
                return e.value;
            }
            prev = e;
            e = next;
        }

        return null;
    }

    /**
     * Copies all of the mappings from the specified map to this map.
     *
     * @param m mappings to be stored in this map
     * @throws NullPointerException if the specified map is null
     */
    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        int n = m.size();
        if (n == 0)
            return;

        if (n> threshold) {
            int targetCapacity = (int)(n/ loadFactor + 1);
            if (targetCapacity > MAXIMUM_CAPACITY)
                targetCapacity = MAXIMUM_CAPACITY;

            int newCapacity = entryTable.size();
            while (newCapacity < targetCapacity)
                newCapacity <<= 1;

            if (newCapacity > entryTable.size())
                resize(newCapacity);
        }

        for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
            put(e.getKey(), e.getValue());
    }

    /**
     * Entry class for creating an ArrayList of Entry
     * @param <K> Key of Entry
     * @param <V> Value of Entry
     */
    static class Entry<K,V> implements Map.Entry<K,V> {
        final K key;
        V value;
        Entry<K, V> next;
        final int hash;

        //Creates new Entry
        Entry(int h, K k, V v, Entry<K, V> n) {
            value = v;
            next = n;
            key = k;
            hash = h;
        }

        /**
         *
         * @return Returns key of the associated entry
         */
        public final K getKey() {
            return key;
        }

        /**
         *
         * @return Returns value of the associated entry
         */
        public final V getValue() {
            return value;
        }

        /**
         * Updates the existing value of associated entry
         *
         * @param newValue Returns updated value in map
         * @return oldValue Returns previous value in map
         */
        public final V setValue(V newValue) {
            V oldValue = value;
            value = newValue;
            return oldValue;
        }
    }

    /**
     * Delete all content of map
     */
    @Override
    public void clear() {
        for (int i = 0; i < entryTable.size(); i++)
            entryTable.set(i,null);
        size = 0;
    }

    /**
     *
     * @return Set of keys in map
     */
    public Set<K> keySet() {
        return entryTable.stream().filter(Objects::nonNull).map(Entry::getKey).collect(Collectors.toSet());
    }

    /**
     *
     * @return Set of Values present in map
     */
    public Collection<V> values() {
        return entryTable.stream().filter(Objects::nonNull).map(Entry::getValue).collect(Collectors.toSet());
    }

    public Set<Map.Entry<K, V>> entrySet() {
        return entryTable.stream().filter(Objects::nonNull).collect(Collectors.toSet());
    }

    /**
     *
     * @return Map contents in form of String({Key : Value})
     */
    public String toString() {
        return entryTable.stream()
                .filter(Objects::nonNull)
                .map(entry -> "{" + entry.getKey() + " : " + entry.getValue() + "}")
                .collect(Collectors.joining(","));
    }
}
